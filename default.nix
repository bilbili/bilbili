with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "env";

  src = lib.cleanSource ./.;

  buildInputs = [
    (jekyll.override { withOptionalDependencies = true; })
    glibcLocales
  ];

  buildPhase = ''
    jekyll build
  '';

  installPhase = ''
    mkdir -p $out
    cp -R _site/* $out
  '';
}
}
