with import <nixpkgs> {};

let
  env = bundlerEnv rec {
    name = "kissdent-env";
    gemfile = ./Gemfile;
    lockfile = ./Gemfile.lock;
    gemset = ./gemset.nix;
    # ruby = ruby_2_6;
    inherit ruby;
  };
  tidying = writeScript "tidying" ''
      for file in $(find _site/ -type f -name "*.html"); do
        ${pkgs.htmlTidy}/bin/tidy -imcq -ashtml -utf8 $file
      done
    '';
in
  stdenv.mkDerivation rec {
    name = "kissdent-env";
    buildInputs = [
      env
      ruby
      bundler
      # tidying
    ];

    shellHook = ''
      # exec ${env}/bin/jekyll serve --watch
    '';
  }
